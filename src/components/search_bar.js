import React, { Component } from "react";

/*class based component
    needs a render method otherwise returns an error
*/
class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = { term: '' };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleKey = this.handleKey.bind(this);
    
  }
  render() {
    return (
      <div className="search-bar">
        <input
          value={this.state.term}
          onChange={this.handleInputChange}
          onKeyPress={this.handleKey}
        />
      </div>
    );
  }
  handleKey(event) {
    if (event.key === 'Enter') {
      this.props.onSearchTermChange({term: event.target.value});
    }
  }

  handleInputChange(event) {
    this.setState({ term: event.target.value });
  }
}

export default SearchBar;
