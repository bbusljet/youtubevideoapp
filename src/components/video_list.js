import React from "react";
import VideoListItem from "./video_list_item";

//takes videos array from the root component (App)
const VideoList = (props) => {
    /*props -> the argument we passed in the App component arrives as an argument to the function. 
      In a functional component the props object is an argument. 
      In a class based component props are available anywhere in any method we define as this.props
    */
  const videoItems = props.videos.map(video => {
    return (
      <VideoListItem /*VideoList takes onVideoSelect as props and passes it do VideoListItem*/
        onVideoSelect ={props.onVideoSelect}
        key={video.etag} 
        video={video} />
    );
  });
  
  return <ul className="col-md-4 list-group">
      {videoItems}
      </ul>;
};

export default VideoList;
