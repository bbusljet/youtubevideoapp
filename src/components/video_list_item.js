import React from 'react';

/*the ({video}) is an ES6 feature called destructuring, the argument has a property called video,
 grab it a put it in a variable called video, its the same as const video = props.video
 we also pass onVideoSelect property from video_list component and then call it inside onClick
*/
const VideoListItem = ({video, onVideoSelect}) => {
    const imgUrl = video.snippet.thumbnails.default.url;

    return (
        <li onClick={() => onVideoSelect(video)} className="list-group-item">
            <div className="video-list media">
                <div className="media-left">
                    <img className="media-object" src={imgUrl} />
                </div>

                <div className="media-body">
                    <div className="media-heading">{video.snippet.title}</div>
                </div>
            </div>
        </li>
    );
};

export default VideoListItem;