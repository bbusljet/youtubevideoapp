import React, { Component } from "react";
import ReactDOM from "react-dom";
import SearchBar from "./components/search_bar";
import apiKeys from "D:/repos/apiKeys";
import YTSearch from "youtube-api-search";
import VideoList from "./components/video_list";
import VideoDetail from "./components/video_details";

const API_KEY = apiKeys.youtube;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };
    this.videoSearch = this.videoSearch.bind(this);
    this.videoSearch('football'); //initial search
  }

  videoSearch(term) {
    YTSearch({ key: API_KEY, term: term }, videos => {
      this.setState({
        videos: videos,
        selectedVideo: videos[0]
      });
    });
  }

  render() {
    return (
      <div>
        <SearchBar onSearchTermChange={this.videoSearch} />
        {/*passing data from the root component(App) to a child component(VideoList) is done
        by defining a property(videos) and referencing a variable(this.state.videos) -> usually called passing props*/}
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList
          /*pass onVideoSelect to VideoList as props*/
          onVideoSelect={selectedVideo => this.setState({ selectedVideo })}
          videos={this.state.videos}
        />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("container"));
